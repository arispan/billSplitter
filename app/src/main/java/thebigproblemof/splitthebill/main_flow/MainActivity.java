package thebigproblemof.splitthebill.main_flow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import thebigproblemof.splitthebill.R;
import thebigproblemof.splitthebill.utils.AppConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mAmountEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        mAmountEditText = findViewById(R.id.amountEditText);
        Button mCalculateAmountButtton = findViewById(R.id.calculateButton);
        mCalculateAmountButtton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.calculateButton:
                if (!mAmountEditText.getText().toString().isEmpty()) {
                    Intent intent = new Intent(view.getContext(), CalculationsActivity.class);
                    intent.putExtra(AppConstants.IntentFilters.BILL_AMOUNT, (Double) Double.parseDouble(
                            mAmountEditText.getText().toString()));
                    startActivity(intent);
                } else
                    Toast.makeText(MainActivity.this, R.string.please_enter_amount, Toast.LENGTH_SHORT)
                            .show();

                break;
        }
    }
}
