package thebigproblemof.splitthebill.main_flow;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import thebigproblemof.splitthebill.R;
import thebigproblemof.splitthebill.utils.AppConstants;
import thebigproblemof.splitthebill.utils.SplitTheBillTextWatcher;

public class CalculationsActivity extends AppCompatActivity implements
        SeekBar.OnSeekBarChangeListener, AdapterView.OnItemSelectedListener,
        SplitTheBillTextWatcher.TextWatcherListener {

    private EditText tipEditText;
    private TextView finalValue;
    private Double billAmount;
    private int people = 2;
    private SeekBar seekBar;
    private List<String> peopleList;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, android.R.anim.fade_out);
        billAmount = getIntent().getExtras().getDouble(AppConstants.IntentFilters.BILL_AMOUNT);
        setContentView(R.layout.activity_calculations);
        initList();
        initViews();

    }

    private void initViews() {
        seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(this);
        tipEditText = findViewById(R.id.tipEditText);
        tipEditText.addTextChangedListener(new SplitTheBillTextWatcher(this));
        finalValue = findViewById(R.id.finalValue);
        spinner = findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        initAdapterForSpinner();
    }

    private void initAdapterForSpinner() {
        ArrayAdapter<String> dataAdapter = new
                ArrayAdapter<>(this, android.R.layout.simple_spinner_item, peopleList);

        spinner.setAdapter(dataAdapter);
    }

    private void initList() {
        peopleList = new ArrayList<>();

        for (int i = 1; i <= 10; i++) {
            peopleList.add(String.valueOf(i));
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        tipEditText.setText(String.valueOf(i));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        setValuesToViews();

    }

    private void setValuesToViews() {
        StringBuilder builder = new StringBuilder();
        builder.append("For ").append(people).append(people == 1 ? " person" : " persons")
                .append(" the final amount with tip is ").append(
                (float) ((billAmount + seekBar.getProgress()) / people))
                .append(" per person");
        finalValue.setText(builder);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        people = Integer.valueOf(peopleList.get(i));
        setValuesToViews();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void userIsWriting(String text) {

        if (tipEditText.getText().length() > 0) {
            tipEditText.setSelection(tipEditText.getText().length());
        }

        if (!text.isEmpty())
            if ((int) Double.parseDouble(text) > 100)
                tipEditText.setText(R.string.default_value);
            else
                seekBar.setProgress((int) Double.parseDouble(text));
        else
            seekBar.setProgress(0);

        setValuesToViews();
    }
}
