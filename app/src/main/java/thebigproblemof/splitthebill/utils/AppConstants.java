package thebigproblemof.splitthebill.utils;

/**
 * Created by arispanayiotou on 22/12/2017.
 */

public class AppConstants {

    public class IntentFilters{
        public static final String BILL_AMOUNT = "bill_amount";
    }

}
