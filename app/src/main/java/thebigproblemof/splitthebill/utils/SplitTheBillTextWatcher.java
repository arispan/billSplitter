package thebigproblemof.splitthebill.utils;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by arispanayiotou on 22/12/2017.
 */

public class SplitTheBillTextWatcher implements TextWatcher {

    public interface TextWatcherListener {
        void userIsWriting(String text);
    }

    private TextWatcherListener mlistener;

    public SplitTheBillTextWatcher(TextWatcherListener listener) {
        this.mlistener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (mlistener != null)
            mlistener.userIsWriting(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

}
